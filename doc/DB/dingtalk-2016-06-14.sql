-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2016-06-14 13:31:53
-- 服务器版本： 5.6.29-log
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dingtalk`
--

-- --------------------------------------------------------

--
-- 表的结构 `dt_approve`
--

CREATE TABLE `dt_approve` (
  `id` int(11) NOT NULL,
  `unionid` varchar(50) NOT NULL COMMENT 'unionid',
  `approve_class` int(10) NOT NULL COMMENT '审批类型',
  `approve_id` int(10) NOT NULL COMMENT '审批id',
  `userid` varchar(50) NOT NULL COMMENT '审批人userid',
  `addtime` varchar(50) NOT NULL COMMENT '添加时间',
  `allow` int(10) NOT NULL DEFAULT '0' COMMENT '是否审批',
  `allower` varchar(50) DEFAULT NULL COMMENT '审批人',
  `allownote` text COMMENT '审批备注',
  `allowtime` varchar(50) DEFAULT NULL COMMENT '审批时间',
  `remove` int(10) NOT NULL DEFAULT '0' COMMENT '是否销假',
  `removenote` text,
  `removetime` varchar(50) DEFAULT NULL COMMENT '销假时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_approve`
--

INSERT INTO `dt_approve` (`id`, `unionid`, `approve_class`, `approve_id`, `userid`, `addtime`, `allow`, `allower`, `allownote`, `allowtime`, `remove`, `removenote`, `removetime`) VALUES
(1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 0, 1, 'manager4617', '1465834688', 1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看见了', NULL, 0, NULL, NULL),
(2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, 1, 'manager4617', '1465836243', 2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '看看', NULL, 0, NULL, NULL),
(3, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 2, 1, 'manager4617', '1465834669', 0, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '不', NULL, 0, NULL, NULL),
(4, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', 2, 2, '032862441032987435', '1465633894', 0, NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dt_business`
--

CREATE TABLE `dt_business` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `address` varchar(100) NOT NULL,
  `start` varchar(50) NOT NULL COMMENT '开始时间',
  `end` varchar(50) NOT NULL COMMENT '结束时间',
  `how` int(50) NOT NULL COMMENT '请假天数',
  `why` text NOT NULL COMMENT '请假事由',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_business`
--

INSERT INTO `dt_business` (`id`, `name`, `address`, `start`, `end`, `how`, `why`, `pic`) VALUES
(1, '麦青强', '北京', '1465488000', '1465488000', 1, '55', '/dingtalk/Uploads/20160610/575a94873c88a.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `dt_leave`
--

CREATE TABLE `dt_leave` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `class` int(10) NOT NULL COMMENT '请假类型',
  `start` varchar(50) NOT NULL COMMENT '开始时间',
  `end` varchar(50) NOT NULL COMMENT '结束时间',
  `how` int(50) NOT NULL COMMENT '请假天数',
  `why` text NOT NULL COMMENT '请假事由',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_leave`
--

INSERT INTO `dt_leave` (`id`, `name`, `class`, `start`, `end`, `how`, `why`, `pic`) VALUES
(1, '麦青强', 1, '1465510740', '1465510740', 3, '555', '/dingtalk/Uploads/20160610/575a945e6e9a3.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `dt_leave_class`
--

CREATE TABLE `dt_leave_class` (
  `id` int(11) NOT NULL,
  `classid` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_leave_class`
--

INSERT INTO `dt_leave_class` (`id`, `classid`, `name`) VALUES
(1, '1', '事假'),
(2, '2', '病假'),
(3, '3', '年假'),
(4, '4', '调休'),
(5, '5', '婚假'),
(6, '6', '产假'),
(7, '7', '陪产假'),
(8, '8', '路途假'),
(9, '0', '其他');

-- --------------------------------------------------------

--
-- 表的结构 `dt_notice`
--

CREATE TABLE `dt_notice` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT '公告标题',
  `content` text NOT NULL COMMENT '公告内容',
  `unionid` varchar(100) NOT NULL COMMENT '发布人唯一标识',
  `username` varchar(100) NOT NULL COMMENT '发布人',
  `time` int(50) NOT NULL COMMENT '发布时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_notice`
--

INSERT INTO `dt_notice` (`id`, `title`, `content`, `unionid`, `username`, `time`) VALUES
(1, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '小明', 1464437916),
(2, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '小红', 1464427916),
(3, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '小明', 1464437916),
(4, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '小红', 1464427916),
(5, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '小明', 1464437916),
(6, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '小红', 1464427916),
(7, '明天放假', '明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假明天放假', '123456789', '小明', 1464437916),
(8, '年底分红通知', '年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知年底分红通知', '123456788', '小红', 1464427916);

-- --------------------------------------------------------

--
-- 表的结构 `dt_read_notice`
--

CREATE TABLE `dt_read_notice` (
  `id` int(11) NOT NULL,
  `unionid` varchar(50) NOT NULL,
  `notice_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `time` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_read_notice`
--

INSERT INTO `dt_read_notice` (`id`, `unionid`, `notice_id`, `name`, `time`) VALUES
(1, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 1, '麦青强', 1464594036),
(2, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 5, '麦青强', 1464594077),
(3, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 3, '麦青强', 1464596264),
(4, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 7, '麦青强', 1464596716),
(5, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 2, '麦青强', 1464596719),
(6, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 4, '麦青强', 1464596731),
(7, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', 1, '苗志锋', 1464597917),
(8, 'lvJoS6szpWkiE', 1, '吴堂福', 1464772336),
(9, 'lvJoS6szpWkiE', 3, '吴堂福', 1464772346),
(10, 'lvJoS6szpWkiE', 5, '吴堂福', 1464772355),
(11, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', 2, '苗志锋', 1464783711),
(12, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 6, '麦青强', 1465203240),
(13, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', 8, '麦青强', 1465203529),
(14, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', 8, '苗志锋', 1465633752);

-- --------------------------------------------------------

--
-- 表的结构 `dt_reimburse`
--

CREATE TABLE `dt_reimburse` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `money` varchar(50) NOT NULL,
  `classname` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `dt_reimburse`
--

INSERT INTO `dt_reimburse` (`id`, `name`, `money`, `classname`, `details`, `pic`) VALUES
(1, '麦青强', '225.55', '图标', '看看', '/dingtalk/Uploads/20160610/575a94b9d5207.jpg'),
(2, '苗志锋', '100', '出差', '差旅100', '/dingtalk/Uploads/');

-- --------------------------------------------------------

--
-- 表的结构 `dt_sign`
--

CREATE TABLE `dt_sign` (
  `id` int(11) NOT NULL,
  `unionid` varchar(100) NOT NULL COMMENT 'unionid',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `address` varchar(100) NOT NULL COMMENT '地址',
  `latitude` varchar(50) NOT NULL COMMENT '纬度',
  `longitude` varchar(50) NOT NULL COMMENT '经度',
  `time` varchar(50) NOT NULL COMMENT '签到时间',
  `note` text COMMENT '备注',
  `pic` varchar(100) DEFAULT NULL COMMENT '图片'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签到';

--
-- 转存表中的数据 `dt_sign`
--

INSERT INTO `dt_sign` (`id`, `unionid`, `name`, `address`, `latitude`, `longitude`, `time`, `note`, `pic`) VALUES
(7, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489653', 'tyhr', '/dingtalk/Uploads/20160610/575998ff0109e.jpg'),
(8, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489664', 'dfsg', '/dingtalk/Uploads/20160610/5759999d4da05.jpg'),
(9, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465489823', 'sdaf', '/dingtalk/Uploads/20160610/57599a5fe6bb8.jpg'),
(10, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490018', 'ds ', ''),
(11, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490101', '545', ''),
(12, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490150', '', ''),
(13, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490296', '', ''),
(14, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490365', '565', '/Uploads/20160610/57599bcc5066e.jpg'),
(15, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490381', '', ''),
(16, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490644', '', '20160610/57599d3e7649e.jpg'),
(17, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465490752', '', '/dingtalk/Upload/20160610/57599e5d6e446.jpg'),
(18, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465491074', '', '/dingtalk/Upload/20160610/57599e8c1760a.jpg'),
(19, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207882', '108.184408', '1465491181', '', '/dingtalk/Uploads/20160610/57599ef648f8c.jpg'),
(20, 'FbiiMKdHyCkkaZDY6iiQZuoAiEiE', '麦青强', '广西壮族自治区南宁市武鸣区珠江路靠近广西水利电力职业技术学院卫生所', '23.207884', '108.184406', '1465536700', '', '/dingtalk/Uploads/20160610/575a52ef53d4b.jpg'),
(21, 'mlpPWiS8ZnzY6o2BPwR32EgiEiE', '苗志锋', '广西壮族自治区南宁市兴宁区长堽路三里二里二巷靠近广西师范学院-田径足球运动场', '22.836381', '108.351102', '1465633767', '', '/dingtalk/Uploads/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dt_approve`
--
ALTER TABLE `dt_approve`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_business`
--
ALTER TABLE `dt_business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_leave`
--
ALTER TABLE `dt_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_leave_class`
--
ALTER TABLE `dt_leave_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_notice`
--
ALTER TABLE `dt_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_read_notice`
--
ALTER TABLE `dt_read_notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_reimburse`
--
ALTER TABLE `dt_reimburse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dt_sign`
--
ALTER TABLE `dt_sign`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `dt_approve`
--
ALTER TABLE `dt_approve`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `dt_business`
--
ALTER TABLE `dt_business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dt_leave`
--
ALTER TABLE `dt_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `dt_leave_class`
--
ALTER TABLE `dt_leave_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- 使用表AUTO_INCREMENT `dt_notice`
--
ALTER TABLE `dt_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- 使用表AUTO_INCREMENT `dt_read_notice`
--
ALTER TABLE `dt_read_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- 使用表AUTO_INCREMENT `dt_reimburse`
--
ALTER TABLE `dt_reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `dt_sign`
--
ALTER TABLE `dt_sign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
