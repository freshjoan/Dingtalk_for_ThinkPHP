<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/6/1 0001
 * Time: 18:09
 */

namespace Home\Controller;

/**
 * 签到功能
 * Class SignController
 * @package Home\Controller
 */

class SignController extends ComController
{
    /**
     * 签到首页
     */
    public function index(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);
        $this->assign("NowTime",time());
//        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
//        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $this->display();

    }


    /**
     * 提交签到
     */
    public function add(){
        $info=upload();
        $data['unionid']=session("unionid");
        $data['name']=getUser(getUseridByUnionid($data["unionid"]))->name;
        $data['address']=I("post.address");
        $data['latitude']=I("post.latitude");
        $data['longitude']=I("post.longitude");
        $data['time']=I("post.time");
        $data['note']=I("post.note");
        $data['pic']=empty($info['pic']['savename'])?"":C("file_upload").$info['pic']['savepath'].$info['pic']['savename'];
        $Sign=M("sign");
//        dump($data);
        if ($Sign->add($data)){
            $this->success("签到成功!",U('index')."?dd_nav_bgcolor=FF5E97F6");
        }else{
            $this->error("签到失败!", $_SERVER['HTTP_REFERER']."?dd_nav_bgcolor=FF5E97F6");
        }
    }

    /**
     * 签到详情
     */
    public function details(){
        $this->display();
    }

    /**
     * 查看本人的签到情况
     */
    public function self(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $Sign=M("sign");
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $where["time"]=array(
            array("egt",$beginToday),
            array("elt",$endToday)
        );
        $where["unionid"]=session("unionid");

        $ret=$Sign->where($where)->order("time desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 查看其他人的签到情况
     */
    public function others(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $Sign=M("sign");
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $where["time"]=array(
            array("egt",$beginToday),
            array("elt",$endToday)
        );
        $ret=$Sign->where($where)->order("time desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }
}