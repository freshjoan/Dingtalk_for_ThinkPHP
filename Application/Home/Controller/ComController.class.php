<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/5/28 0028
 * Time: 18:40
 */

namespace Home\Controller;
use Think\Controller;

/**
 * 公共控制器
 * Class ComController
 * @package Home\Controller
 */
class ComController extends Controller{
    public function _initialize(){
        OauthDingTalk(curPageURL());
        if (session('?unionid')){
            $isAdmin=isDingTalkAdmin(session('unionid'));
            session("isAdmin",$isAdmin);
        }
    }
}