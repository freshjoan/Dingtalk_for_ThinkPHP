<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>出差</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    
</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);">
                    
                        <a href="javascript:" onclick="history.back();" class="back link">
                            <i class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i>
                            <span class="">返回</span>
                        </a>
                    

                </div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">我的出差</div>
                    <div class="right">
                        
                    </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    
    <div class="list-block">
        <form>
            <ul>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">出差地点</div>
                            <div class="item-input">
                                <input type="text" placeholder="如：北京、上海、杭州(必填)">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">开始时间</div>
                            <div class="item-input">
                                <input type="date" placeholder="Birth day" value="2014-04-30" class="">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">结束时间</div>
                            <div class="item-input">
                                <input type="date" placeholder="Birth day" value="2014-04-30" class="">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">出差天数(天)</div>
                            <div class="item-input">
                                <input type="text" placeholder="如：北京、上海、杭州(必填)">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="align-top">
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">出差事由</div>
                            <div class="item-input">
                                <textarea placeholder="请输入出差事由(必填)"></textarea>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="align-top">
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">图片</div>
                            <div class="item-input">
                                <input type="file" name="" id="" style="padding-top: 10px;">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="align-top">
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">审批人</div>
                            <div class="item-after"><i class="iconfont icon-tianjia"></i></div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="content-block">
                <input class="button button-big button-fill color-green" type="submit" value="提交">
            </div>
        </form>
    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/AmazeUI/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->

</body>
</html>