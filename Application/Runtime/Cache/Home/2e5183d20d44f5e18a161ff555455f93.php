<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">
    <title>已读的人</title>

    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

    <!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="icon" type="image/png" href="/dingtalk/Public/Home/AmazeUI/i/favicon.png">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="apple-touch-icon-precomposed" href="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">
    <meta name="msapplication-TileColor" content="#0e90d2">

    <link rel="stylesheet" href="/dingtalk/Public/Home/AmazeUI/css/amazeui.min.css">
    <link rel="stylesheet" href="/dingtalk/Public/Home/AmazeUI/css/app.css">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_1464435241_5855658.css">
    <style>
        .notice a i {
            margin-right: 1rem;
        }

        /*.notice a {*/
        /*color: #000000;*/
        /*}*/
        .icon-weiduyoujian {
            color: red;
        }

        .icon-yiduyoujian {
            color: #5c5c5c;
        }
        .am-list li a{
            margin-left: 1rem;
        }
    </style>

</head>
<body>
<header data-am-widget="header"
        class="am-header am-header-default">
    <div class="am-header-left am-header-nav">
        <a href="#left-link" class="">

            <i class="am-header-icon am-icon-chevron-left"></i>
        </a>
    </div>

    <h1 class="am-header-title">
        <a href="#title-link" class="">
            已读的人
        </a>
    </h1>
</header>
<div class=" am-titlebar-multi" >
    <h2 class="am-titlebar-title ">
        科技频道
    </h2>
    <nav class="am-titlebar-nav">
        <a href="#more-1" class="">已读人数：100</a>
        <a href="#more-2" class="">查看谁读</a>
    </nav>
</div>
<div class="notice">

    <div class="am-list-news-bd">
        <ul class="am-list">
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="am-g am-list-item-dated">
                    <a href="##" class="am-list-item-hd"><?php echo ($vo["name"]); ?></a>
                    <span class="am-list-date"><?php echo date('Y-m-d h:i:s',$vo['time']);?></span>
                </li><?php endforeach; endif; else: echo "" ;endif; ?>

        </ul>
    </div>
</div>

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/AmazeUI/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="/dingtalk/Public/Home/AmazeUI/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
<script src="/dingtalk/Public/Home/AmazeUI/js/amazeui.min.js"></script>
</body>
</html>