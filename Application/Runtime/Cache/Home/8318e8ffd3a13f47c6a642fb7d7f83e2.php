<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">
    <title>公告</title>

    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

    <!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="icon" type="image/png" href="/dingtalk/Public/Home/AmazeUI/i/favicon.png">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="apple-touch-icon-precomposed" href="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="/dingtalk/Public/Home/AmazeUI/i/app-icon72x72@2x.png">
    <meta name="msapplication-TileColor" content="#0e90d2">

    <link rel="stylesheet" href="/dingtalk/Public/Home/AmazeUI/css/amazeui.min.css">
    <link rel="stylesheet" href="/dingtalk/Public/Home/AmazeUI/css/app.css">
</head>
<body>
<header data-am-widget="header"
        class="am-header am-header-default">
    <div class="am-header-left am-header-nav">
        <a href="javascript:" onclick="history.back();" class="">

            <i class="am-header-icon am-icon-chevron-left"></i>
        </a>
    </div>

    <h1 class="am-header-title">
        <a href="#title-link" class="">
            公告
        </a>
    </h1>

    <!--<div class="am-header-right am-header-nav">-->
    <!--<a href="#right-link" class="">-->
    <!--<i class="am-header-icon am-icon-bars"></i>-->
    <!--</a>-->
    <!--</div>-->
</header>
<div class="am-intro" style="margin: 0 0.5rem">

    <article class="am-article">
        <div class="am-article-hd">
            <h1 class="am-article-title" align="center"><?php echo ($ret["title"]); ?></h1>
            <p class="am-article-meta">发布人:<?php echo ($ret["username"]); ?> &nbsp; 时间:<?php echo date('y-m-d H:i:s',$ret['time']);?> &nbsp; <a href="#">浏览数:100</p>
        </div>

        <div class="am-article-bd" style="color: #000">
            <?php echo ($ret["content"]); ?>
        </div>
    </article>

</div>


<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/AmazeUI/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="/dingtalk/Public/Home/AmazeUI/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->
<script src="/dingtalk/Public/Home/AmazeUI/js/amazeui.min.js"></script>
</body>
</html>