<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>其他人今日的签到情况</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    

</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);">
                    
                        <a href="javascript:" onclick="history.back();" class="back link">
                            <i class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i>
                            <span class="">返回</span>
                        </a>
                    

                </div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">其他人今日的签到情况</div>
                    <div class="right">
                        

                    </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    
    <div class="list-block accordion-list">
        <ul>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><li class="accordion-item"><a href="#" class="item-content item-link">
                    <div class="item-inner">
                        <div class="item-title">Item 1</div>
                    </div></a>
                    <div class="accordion-item-content">
                        <div class="content-block">
                            <ul>
                                <li class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title">姓名</div>
                                        <div class="item-after"><?php echo ($v["name"]); ?></div>
                                    </div>
                                </li>
                                <li class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title">地址：</div>
                                        <div class=""><?php echo ($v["address"]); ?>地址地址地址地址地址地址地址地址地址地址地址地址地址地址地址地址地址</div>
                                    </div>
                                </li>
                                <li class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title">签到时间：</div>
                                        <div class="item-after"><?php echo date("Y-m-d H:i:s",$v['name']);?></div>
                                    </div>
                                </li>
                            </ul>
                            <p>

                            </p>
                        </div>
                    </div>
                </li><?php endforeach; endif; else: echo "" ;endif; ?>
            <li class="accordion-item"><a href="#" class="item-content item-link">
                <div class="item-inner">
                    <div class="item-title">Item 1</div>
                </div></a>
                <div class="accordion-item-content">
                    <div class="content-block">
                        <ul>
                            <li class="item-content">
                                <div class="item-inner">
                                    <div class="item-title">姓名</div>
                                    <div class="item-after"><?php echo ($v["name"]); ?></div>
                                </div>
                            </li>
                            <li class="item-content">
                                <div class="item-inner">
                                    <div class="item-title-row">
                                        <div class="item-title">地址</div>
                                    </div>
                                    <div class="content-block"><?php echo ($v["address"]); ?></div>
                                </div>
                            </li>
                            <li class="item-content">
                                <div class="item-inner">
                                    <div class="item-title">签到时间</div>
                                    <div class="item-after"><?php echo date("Y-m-d H:i:s",$v['name']);?></div>
                                </div>
                            </li>
                            <li class="item-content">
                                <div class="item-inner">
                                    <div class="item-title-row">
                                        <div class="item-title">备注：</div>
                                    </div>
                                    <div class="content-block"><img src="http://www.thinkphp.cn/Uploads/da/2016-04-13/570db53c0d1a3.gif"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>


                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->



</body>
</html>