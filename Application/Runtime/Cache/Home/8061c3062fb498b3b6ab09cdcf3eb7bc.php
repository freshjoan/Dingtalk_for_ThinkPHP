<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>预约会议室</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    
    <style>
        .item-after .badge{
            margin: 0 2px;

        }
        .content-block-title h2{
            color: #3cc51f;
        }
        .time{
            font-size: 14px;
        }
    </style>

</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);">
                    
                        <a href="javascript:" onclick="history.back();" class="back link">
                            <i class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i>
                            <span class="">返回</span>
                        </a>
                    

                </div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">预约会议室</div>
                    <div class="right">
                        
    <a href="#" class="link external">
        <span class="">马上预约</span>
    </a>

                    </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    
    <div class="content-block-title"><h2>第一会议室</h2></div>
    <div class="list-block">
        <ul>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title">位置</div>
                    <div class="item-after">办公楼一楼a501</div>
                </div>
            </li>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title">楼层</div>
                    <div class="item-after">一楼</div>
                </div>
            </li>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title">最大容纳人数</div>
                    <div class="item-after">100</div>
                </div>
            </li>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title">是否支持WIFI</div>
                    <div class="item-after"><span class="badge color-green">YES</span></div>
                </div>
            </li>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title">是否支持投影</div>
                    <div class="item-after"><span class="badge color-green">YES</span></div>
                </div>
            </li>
            <li class="accordion-item accordion-item-expanded"><a href="#" class="item-content item-link">
                <div class="item-inner">
                    <div class="item-title">其他</div>
                </div></a>
                <div class="accordion-item-content ">
                    <div class="content-block">
                        <p>快乐萨哈夫卢卡斯的解放路开骂</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="list-block">
        <div class="content-block-title">预约情况</div>
        <ul>
            <li class="item-content">
                <div class="item-inner">
                    <div class="item-title time">2013-10-14 15:15 ~ 2013-10-14 15:15</div>
                    <div class="item-after">已被预约</div>
                </div>
            </li>
        </ul>
    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/AmazeUI/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->




</body>
</html>